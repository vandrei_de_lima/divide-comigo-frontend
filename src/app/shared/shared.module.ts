import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [],
  exports:[CommonModule,TranslateModule]

})
export class SharedModule { }
