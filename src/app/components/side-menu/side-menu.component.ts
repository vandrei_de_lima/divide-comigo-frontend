import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'dc-side-navigation-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.less'],
})
export class SideMenuNavigationComponent implements OnInit {
  @Input() links: any[] = [];

  @Output() sideMenu = new EventEmitter<boolean>();

  isOpen: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
