import { AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'dc-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class InputComponent {
  @Input() formControlInput!: AbstractControl;
  @Input() formControlConfirmPassword!: AbstractControl;

  message1: string = '';
  message2: string = '';
  invalidEmail: string = '';

  constructor(private translate: TranslateService) {}

  checkValidation(): any {
    if (
      this.formControlInput &&
      this.formControlInput.touched &&
      !this.formControlInput.valid &&
      this.formControlInput.errors
    ) {
      const errors: any = Object.keys(this.formControlInput.errors);
      for (let index = 0; index < errors.length; index++) {
        const typeValidation: string = errors[index];
        const isInvalid: boolean = this.formControlInput.errors[typeValidation];

        if (isInvalid) {
          switch (typeValidation) {
            case 'email':
              return this.translate.instant('email-invalid');

            case 'required':
              return this.translate.instant('required_field');

            case 'equalsTo':
              return this.translate.instant('password_not_equals');
          }
        }
      }
    }
  }
}
