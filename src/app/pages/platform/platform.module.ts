import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { PlatformComponent } from './platform.component';
import { ExplorerModule } from './explorer/explorer.module';
import { MyGroupsModule } from './my-groups/my-groups.module';
import { FinancialModule } from './financial/financial.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlatformRoutingModule } from './platform-routing.module';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { CreateGroupsModule } from './create-groups/create-groups.module';
import { SideMenuModule } from './../../components/side-menu/side-menu.module';
import { HeaderPlatformComponent } from './_header-platform/header-platform.component';

@NgModule({
  declarations: [PlatformComponent, SideMenuComponent, HeaderPlatformComponent],
  imports: [
    FormsModule,
    CommonModule,
    MyGroupsModule,
    ExplorerModule,
    SideMenuModule,
    TranslateModule,
    FinancialModule,
    CreateGroupsModule,
    ReactiveFormsModule,
    PlatformRoutingModule,
  ],
  exports: [TranslateModule],
})
export class PlatformModule {}
