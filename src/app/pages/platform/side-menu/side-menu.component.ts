import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dc-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.less'],
})
export class SideMenuComponent implements OnInit {
  myGroups: boolean = true;
  createGroup: boolean = false;
  explorer: boolean = false;
  finance: boolean = false;
  config: boolean = false;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  clearAllFlags(): void {
    this.myGroups = false;
    this.createGroup = false;
    this.explorer = false;
    this.finance = false;
    this.config = false;
  }

  redirectMyGroups(): void {
    this.clearAllFlags();
    this.myGroups = true;
    this.router.navigate(['platform/my-groups']);
  }

  redirectCreateGroup(): void {
    this.clearAllFlags();
    this.createGroup = true;
    this.router.navigate(['platform/create-groups']);
  }

  redirectExplorer(): void {
    this.clearAllFlags();
    this.explorer = true;
    this.router.navigate(['platform/explorer']);
  }

  redirectFinancial(): void {
    this.clearAllFlags();
    this.finance = true;
    this.router.navigate(['platform/financial']);
  }

  redirectConfiguration(): void {
    this.clearAllFlags();
    this.config = true;
    this.router.navigate(['platform/configuration']);
  }
}
