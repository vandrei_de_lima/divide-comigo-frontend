import { BrowserModule } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { MyGroupsComponent } from './my-groups.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule, TranslateModule],
  declarations: [MyGroupsComponent],
  exports: [TranslateModule],
})
export class MyGroupsModule {}
