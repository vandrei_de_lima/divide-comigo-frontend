import { UserHttpService } from './../../../services/user-http.service';
import { UserService } from './../../../services/user.service';
import { GroupHttpService } from './../../../services/group-http.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeLast, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Group } from '../platform.type';

@Component({
  selector: 'dc-my-groups',
  templateUrl: './my-groups.component.html',
  styleUrls: ['./my-groups.component.less'],
})
export class MyGroupsComponent implements OnInit, OnDestroy {
  myGroups: Group[] = [];
  destroy$ = new Subject<any>();

  constructor(
    private groupHttpService: GroupHttpService,
    private userService: UserService,
    private userHttpService: UserHttpService
  ) {}

  ngOnInit(): void {
    this.userHttpService.userUpdate$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.getUser();
      });
    this.getUser();
  }

  getUser(): void {
    this.groupHttpService
      .getGroupByUser(this.userService.user.id)
      .subscribe((result) => {
        this.myGroups = result.groups;
        console.log(this.myGroups);
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
