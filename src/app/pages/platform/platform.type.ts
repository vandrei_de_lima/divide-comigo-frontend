export type Routers = {
  description: string;
  link: string;
};

export type TypeGroups = {
  description: string;
  id: number;
};

export type Group = {
  administrator: string;
  createdAt: string;
  description: string;
  id: number;
  members: any[];
  membersAmount: number;
  name: string;
  owner: string;
  price: string;
  typeGroup: string;
  updatedAt: string;
};
