import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/services/user.service';
import { UserHttpService } from './../../services/user-http.service';
import { AuthenticationService } from './../authentication/authentication.service';

@Component({
  selector: 'dc-platform',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.less'],
})
export class PlatformComponent implements OnInit {
  constructor(
    private auth: AuthenticationService,
    private userService: UserService,
    private userHttpService: UserHttpService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (!this.userService.user) {
      this.getCurrentUser();
    }
    this.router.navigate(['platform/my-groups']);
  }

  getCurrentUser(): void {
    this.auth.getCurrentUser().then((email) => {
      this.userHttpService.getUser(email).subscribe((user) => {
        this.userService.user = { ...user.user };
        this.userHttpService.userUpdate$.next();
      });
    });
  }

  logout(): void {
    this.auth.logout();
  }
}
