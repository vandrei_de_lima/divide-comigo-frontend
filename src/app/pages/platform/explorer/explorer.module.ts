import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExplorerComponent } from './explorer.component';

@NgModule({
  declarations: [ExplorerComponent],
  imports: [CommonModule, TranslateModule],
  exports: [TranslateModule],
})
export class ExplorerModule {}
