import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinancialComponent } from './financial.component';

@NgModule({
  declarations: [FinancialComponent],
  imports: [CommonModule, TranslateModule],
  exports: [TranslateModule],
})
export class FinancialModule {}
