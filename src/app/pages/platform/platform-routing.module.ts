import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfigurationComponent } from './configuration/configuration.component';
import { FinancialComponent } from './financial/financial.component';
import { ExplorerComponent } from './explorer/explorer.component';
import { CreateGroupsComponent } from './create-groups/create-groups.component';
import { MyGroupsComponent } from './my-groups/my-groups.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlatformComponent } from './platform.component';

const routes: Routes = [
  {
    path: '',
    component: PlatformComponent,
    children: [
      { path: 'my-groups', component: MyGroupsComponent },
      { path: 'create-groups', component: CreateGroupsComponent },
      { path: 'explorer', component: ExplorerComponent },
      { path: 'financial', component: FinancialComponent },
      { path: 'configuration', component: ConfigurationComponent },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlatformRoutingModule {}
