import { InputModule } from './../../../components/input/input.module';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateGroupsComponent } from './create-groups.component';
import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    FormsModule,
    InputModule,
    CommonModule,
    TranslateModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  declarations: [CreateGroupsComponent],
  exports: [TranslateModule, InputModule],
  providers: [CurrencyPipe],
})
export class CreateGroupsModule {}
