import { UserService } from './../../../services/user.service';
import { GroupHttpService } from './../../../services/group-http.service';
import {
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { TypeGroups } from '../platform.type';

@Component({
  selector: 'dc-create-groups',
  templateUrl: './create-groups.component.html',
  styleUrls: ['./create-groups.component.less'],
})
export class CreateGroupsComponent implements OnInit {
  form!: UntypedFormGroup;

  validEmailMember!: UntypedFormGroup;

  members: any[] = [];

  typesGroups: TypeGroups[] = [
    { description: 'video_streaming', id: 1 },
    { description: 'music_streaming', id: 2 },
    { description: 'service_streaming', id: 3 },
    { description: 'other_streaming', id: 4 },
  ];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private groupServiceHttp: GroupHttpService,
    private userService: UserService,
    private currencyPipe: CurrencyPipe
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: [null, [Validators.required]],
      description: [null, []],
      typeGroup: [null, [Validators.required]],
      membersAmount: [null, [Validators.required]],
      price: [null, [Validators.required]],
    });
    this.validEmailMember = this.formBuilder.group({
      email: [null, [Validators.email, Validators.required]],
    });
  }

  createNewGroup(): void {
    this.validateAllFormFields();
    if (this.form.valid) {
      const { name, description, typeGroup, membersAmount, price } =
        this.form.controls;
      const newGroup = {
        name: name.value,
        description: description.value,
        typeGroup: typeGroup.value,
        membersAmount: membersAmount.value,
        price: price.value,
        members: JSON.stringify(this.members),
        administrator: this.userService.user.id,
        owner: this.userService.user.id,
      };

      this.groupServiceHttp.createGroup(newGroup).subscribe((result) => {});
    }
  }

  validateAllFormFields() {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      if (control instanceof UntypedFormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {
        this.validateAllFormFields();
      }
    });
  }

  addNewMember(): void {
    if (this.validEmailMember.valid) {
      this.members.push(this.validEmailMember.controls.email.value);
      this.validEmailMember.reset();
    }
  }

  removeUser(index: number): void {
    this.members.splice(index, 1);
  }

  transformAmount(element: any) {
    let newValue = this.currencyPipe.transform(
      this.form.controls.price.value,
      'R$'
    );

    element.target.value = newValue;
  }
}
