import { Component, OnInit } from '@angular/core';

import { Routers } from '../platform.type';

@Component({
  selector: 'dc-header-platform',
  templateUrl: './header-platform.component.html',
  styleUrls: ['./header-platform.component.less'],
})
export class HeaderPlatformComponent implements OnInit {
  routers: Routers[] = [
    { description: 'my_groups', link: '/platform/my-groups' },
    { description: 'create_groups', link: '/platform/create-groups' },
    { description: 'explorer', link: '/platform/explorer' },
    { description: 'financial', link: '/platform/financial' },
    { description: 'configuration', link: '/platform/configuration' },
  ];

  imgFakeUser: string =
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRH9FLdiyllR-3cAl_KLJR1teCFTOPAHh82GSuA8GLxZwdzRc5N3InG1HlTtqkZ8ZJb1T8&usqp=CAU';

  constructor() {}

  ngOnInit(): void {}
}
