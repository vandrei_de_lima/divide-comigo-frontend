import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import {
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';

import { VerifyUserResult, WrongPassword } from '../authentication.type';
import { UserService } from 'src/app/services/user.service';
import { AuthenticationService } from './../authentication.service';
import { UserHttpService } from './../../../services/user-http.service';

@Component({
  selector: 'dc-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.less'],
})
export class SignInComponent implements OnInit {
  imgFakeUser: string =
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRH9FLdiyllR-3cAl_KLJR1teCFTOPAHh82GSuA8GLxZwdzRc5N3InG1HlTtqkZ8ZJb1T8&usqp=CAU';

  emailCollected: boolean = false;
  emailInvalid: boolean = false;

  wrongPassword: WrongPassword = {
    isIncorrect: false,
    attempt: 0,
  };
  destroy$: Subject<void> = new Subject<void>();

  form!: UntypedFormGroup;

  constructor(
    private auth: AuthenticationService,
    private formBuilder: UntypedFormBuilder,
    public userHttpService: UserHttpService,
    public userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
    });
  }

  verifyEmail(): void {
    const email: string = this.form.controls.email.value;

    if (this.form.controls.email.valid) {
      this.userHttpService
        .verifyUser(email.toLowerCase())
        .subscribe((result: VerifyUserResult) => {
          if (result.isValid) {
            this.emailCollected = true;
            this.userService.userName = result.name;
          } else this.emailInvalid = true;
        });
    }
  }

  validateAllFormFields() {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      if (control instanceof UntypedFormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {
        this.validateAllFormFields();
      }
    });
  }

  login(): void {
    this.validateAllFormFields();

    if (this.form.valid) {
      const email: string = this.form.controls.email.value;
      const password: string = this.form.controls.password.value;

      this.auth.login(email, password).then((result: any) => {
        if (result.user) {
          this.userHttpService.getUser(email).subscribe((result) => {
            this.userService.user = result;
          });

          this.router.navigate(['']);
        } else {
          this.wrongPassword.isIncorrect = true;
          this.wrongPassword.attempt++;
        }
      });
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
