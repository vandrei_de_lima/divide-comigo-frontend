import {
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from './../authentication.service';

@Component({
  selector: 'dc-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.less'],
})
export class SignUpComponent implements OnInit {
  form!: UntypedFormGroup;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private auth: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: [null, [Validators.required]],
      password: [null, [Validators.required, Validators.minLength(8)]],
      confirmPassword: [
        null,
        [
          Validators.required,
          Validators.minLength(8),
          FormValidations.equalsTo('password'),
        ],
      ],
      email: [null, [Validators.required, Validators.email]],
    });
  }

  validateAllFormFields() {
    Object.keys(this.form.controls).forEach((field) => {
      const control = this.form.get(field);
      if (control instanceof UntypedFormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {
        this.validateAllFormFields();
      }
    });
  }

  register(): void {
    this.validateAllFormFields();
    if (this.form.valid) {
      const { email, password, name } = this.form.value;

      this.auth.register(email, password, name).then(() => console.log());
    }
  }
}

export class FormValidations {
  static equalsTo(otherField: string) {
    const validator = (formControl: UntypedFormControl) => {
      if (otherField == null) {
        throw new Error('É necessário informar um campo.');
      }

      if (!formControl.root || !(<UntypedFormGroup>formControl.root).controls) {
        return null;
      }

      const field = (<UntypedFormGroup>formControl.root).get(otherField);

      if (!field) {
        throw new Error('É necessário informar um campo válido.');
      }

      if (field.value !== formControl.value) {
        return { equalsTo: otherField };
      }

      return null;
    };
    return validator;
  }
}
