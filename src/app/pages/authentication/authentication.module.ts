import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { InputModule } from './../../components/input/input.module';
import { AuthenticationRoutingModule } from './authentication-routing.module';

@NgModule({
  imports: [
    FormsModule,
    InputModule,
    CommonModule,
    TranslateModule,
    ReactiveFormsModule,
    AuthenticationRoutingModule,
  ],
  declarations: [SignUpComponent, SignInComponent],
  exports: [TranslateModule],
})
export class AuthenticationModule {}
