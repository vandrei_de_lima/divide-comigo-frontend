export type VerifyUserResult = {
  isValid: boolean;
  name: string;
};

export type WrongPassword = {
  isIncorrect: boolean;
  attempt: number;
};
