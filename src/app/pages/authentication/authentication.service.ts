import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { UserHttpService } from './../../services/user-http.service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  email: string = '';
  password: string = '';

  collectedEmail$: Subject<boolean> = new Subject<boolean>();
  collectedUserAuth$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private router: Router,
    private auth: AngularFireAuth,
    private user: UserHttpService
  ) {}

  async register(email: string, password: string, name: string): Promise<any> {
    this.auth.createUserWithEmailAndPassword(email, password).then(() => {
      this.createUserDataBase(email, name).then(() =>
        this.router.navigate([''])
      );
    });
  }

  async createUserDataBase(email: string, name: string) {
    this.user.createUser(email, name).subscribe(() => true);
  }

  async getCurrentUser(): Promise<any> {
    const user: any = (await this.auth.app).auth().currentUser;

    return user.email;
  }

  async login(email: string, password: string): Promise<any> {
    return await this.auth
      .signInWithEmailAndPassword(email, password)
      .then((result: any) => result)
      .catch((result) => result);
  }

  async logout(): Promise<any> {
    return await this.auth
      .signOut()
      .then(() => this.router.navigate(['auth/signIn']));
  }
}
