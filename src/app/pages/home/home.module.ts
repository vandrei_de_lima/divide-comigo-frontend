import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { SideMenuModule } from './../../components/side-menu/side-menu.module';
import { BlockThirdComponent } from './content/block-third/block-third.component';
import { BlockFirstComponent } from './content/block-first/block-first.component';
import { BlockSecondComponent } from './content/block-second/block-second.component';
import { SeparatorCardsComponent } from './separator-cards/separator-cards.component';

@NgModule({
  imports: [CommonModule, TranslateModule, HomeRoutingModule, SideMenuModule],
  declarations: [
    HomeComponent,
    HeaderComponent,
    ContentComponent,
    BlockThirdComponent,
    BlockFirstComponent,
    BlockSecondComponent,
    SeparatorCardsComponent,
  ],
})
export class HomeModule {}
