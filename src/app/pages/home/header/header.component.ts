import { Component, OnInit } from '@angular/core';
import { Routers } from '../../platform/platform.type';

@Component({
  selector: 'dc-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
})
export class HeaderComponent implements OnInit {
  routers: Routers[] = [
    { description: 'login', link: '/auth/signIn' },
    { description: 'start_to_divide', link: '/auth/signUp' },
  ];

  constructor() {}

  ngOnInit(): void {}
}
