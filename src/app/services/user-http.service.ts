import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserHttpService {
  api: string = '/api';

  nameUser: string = '';

  userUpdate$: Subject<any> = new Subject<any>();
  constructor(private http: HttpClient) {}

  verifyUser(email: string) {
    const url = `${this.api}/users/validEmail?email=${email}`;

    return this.http.get<any>(url);
  }

  getUser(email: string) {
    const url = `${this.api}/users/getUser?email=${email}`;
    return this.http.get<any>(url);
  }

  createUser(email: string, name: string) {
    const url = `${this.api}/users/create`;
    const data = { email, name };

    return this.http.post<any>(url, data);
  }
}
