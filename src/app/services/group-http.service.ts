import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GroupHttpService {
  api: string = '/api';
  constructor(private http: HttpClient) {}

  createGroup(newGroup: any) {
    const url = `${this.api}/groups/create`;

    return this.http.post<any>(url, newGroup);
  }

  getGroupByUser(userId: number) {
    const url = `${this.api}/groups/getGroups?id=${userId}`;
    return this.http.get<any>(url);
  }
}
